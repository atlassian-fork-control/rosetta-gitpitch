---?color=linear-gradient(to right, #c02425, #f0cb35)
@title[Introduction]

@snap[west headline text-white span-70]
Atlassian<br>*Presentation Template*
@snapend

@snap[south-west byline  text-white]
ADG3 guided presentation template
@snapend

---?image=template/img/pencils.jpg
@title[Split-Screen Templates]

## @color[black](Topic Slides)

@fa[arrow-down text-black]

@snap[south docslink span-50]
[Atlassian Design Guidelines](https://atlassian.design/guidelines/marketing/resources/presentation-kit)
@snapend


+++?image=template/img/bg/orange.jpg&position=right&size=50% 100%
@title[Heading + List Body]

@snap[west split-screen-heading text-orange span-50]
Agenda
@snapend

@snap[east text-white span-45]
@ol[split-screen-list](false)
- Agenda Item 1
- Agenda Item 2
- Agenda Item 2
@olend
@snapend

@snap[south-west template-note text-gray]
Slide subtitle in the footer
@snapend